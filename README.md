# devopsassessment

This application connects to a mysql database and searches the users table based on location typed into the search bar.

To run the applicatoin, clone the repo and do the following command: 
	- docker-compose build => Build the application
	- docker-compose up => Runs the application
	- docker-compose up --build => Build and runs the application
	- docker-compose down –rmi all => This shutsdown the application and removes all the images

Then if you go to your localhost:80, the application will be up and running


Team members:
	- Helen
		Created the apache and mysql containers including a bug fix
	- Nyasha
		Created the yaml script and helped with the creation of the containers and bug fixes



